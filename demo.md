# Prep
sudo npm install -g firebase-tools
firebase --version
firebase init emulators
sudo apt-get update && sudo apt-get install -y gnupg software-properties-common
wget -O- https://apt.releases.hashicorp.com/gpg | \
gpg --dearmor | \
sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
gpg --no-default-keyring \
--keyring /usr/share/keyrings/hashicorp-archive-keyring.gpg \
--fingerprint
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] \
https://apt.releases.hashicorp.com $(lsb_release -cs) main" | \
sudo tee /etc/apt/sources.list.d/hashicorp.list
sudo apt update
sudo apt-get install terraform

sudo apt-get install google-cloud-sdk-terraform-tools
sudo apt-get install google-cloud-sdk-config-connector

------------------------------
Matt Demo recording

Clickops
* compute engine
* Create Instance -> Eqivilant code
* Logging and DuetAI
  * `gcloud logging write DUET_DEMO '{"message": "The flux capacitor is broken in both GKE and Cloud Run - you cant go back to the future until its fixed"}' --payload-type=json --severity=ERROR`

gcloud
* `gcloud <blank>`
* gcloud emulator (`gcloud emulators` & `gcloud emulators spanner` & `gcloud alpha emulator`)
* `firebase emulators:start` 
* gcloud build container 
* gcloud export

terraform
* `terraform plan -out=test.tfplan`
* `terraform show -json ./test.tfplan > ./tfplan.json`
* `terraform plan -json > planning.json`
* `gcloud beta terraform vet tfplan.json --policy-library=../policy-library`



Jumpstart solutions


--------------
Cloud Build

Biuld a coninainer image - `gcloud builds submit --region europe-west2 --config ./cloudbuild.yaml .`

```
gcloud builds submit --tag europe-docker.pkg.dev/coffee-and-codey/hello-coffee/coffee:latest
```

Show in Cloud Build
* Show Security Insights and then jump to the artifact

Go to artifact reg and show security 



Cloud Deploy