# Coffee and Codey


What's better to do with a coffee in hand other than code(ey)? This is a demo to show the VertexAI Codey API in action.

The idea is that you leveagre Cloud Build to run a number of CI pipeline steps.

A deck to support the demo and set the scene is [go/devex-duetai](go/devex-duetai) - sorry Google internal only.


# DuetAI in Google Cloud Workstations

## Inline

```
# write a web app to display coffee images
# function to calculate the area of my coffee cup
```
or (incase you cant generate web app due to licesne restrictions)

```
 # write a function to calculate the area of my coffee cup
# write a flask app that serves the area of a coffee cup
```


## Chat
* How are you today and have you had a good coffee yet?
* Write a unit test for code.py
* What is in code.py
* Explain line by line
* how do I deploy my app to cloud run


# Codey in Vertex
* Function in python to upload a file to a Google Cloud Storage bucket using the Cloud Storage Python Client library.
* explain line by line
* how can i improve security?


# Codey in Cloud Build

<!-- create the gcloud syntax to add repo -->
<!-- create the cloud build gcloud syntax -->
<!-- either trigger or manually execute the pipeline -->

To create the Cloud Builder container see [instructions here.](./codey-builder-image/README.md)


## Create the Cloud Build Trigger

The simple demo takes latest and most recent commit and carries out a `git diff` which is outputed to a file. The file is then passed to the codey pyhton sctipt which will create docuemntation for the new code, release notes and suggest some secuirty optimisation.

An example output is;

![Cloud Build demo Output](./static/cloud-build.png)

![Cloud Build demo Output with GenAI aspects](./static/cloud-buid-ai.png)

```
starting build "3f803f4f-d0cb-423f-b232-e0f8a3834f9d"

FETCHSOURCE
hint: Using 'master' as the name for the initial branch. This default branch name
hint: is subject to change. To configure the initial branch name to use in all
hint: of your new repositories, which will suppress this warning, call:
hint: 
hint: 	git config --global init.defaultBranch <name>
hint: 
hint: Names commonly chosen instead of 'master' are 'main', 'trunk' and
hint: 'development'. The just-created branch can be renamed via this command:
hint: 
hint: 	git branch -m <name>
Initialized empty Git repository in /workspace/.git/
From https://gitlab.com/robedwards/coffee-and-codey
 * branch            9ea16642f656818eefdf0f7472a6bc29969c38b5 -> FETCH_HEAD
HEAD is now at 9ea1664 new code for demo
BUILD
Starting Step #0 - "Clone the git repo"
Step #0 - "Clone the git repo": Already have image (with digest): gcr.io/cloud-builders/git
Step #0 - "Clone the git repo": Cloning into './clone'...
Finished Step #0 - "Clone the git repo"
Starting Step #1 - "Compare current and the last version of code"
Step #1 - "Compare current and the last version of code": Pulling image: europe-docker.pkg.dev/coffee-and-codey/codey-builder-image/codey:latest
Step #1 - "Compare current and the last version of code": latest: Pulling from coffee-and-codey/codey-builder-image/codey
Step #1 - "Compare current and the last version of code": ddf874abf16c: Pulling fs layer
Step #1 - "Compare current and the last version of code": 066c91c7b62c: Pulling fs layer
Step #1 - "Compare current and the last version of code": 93be85e41fbb: Pulling fs layer
Step #1 - "Compare current and the last version of code": 64a12208b46c: Pulling fs layer
Step #1 - "Compare current and the last version of code": 5e296e6769f5: Pulling fs layer
Step #1 - "Compare current and the last version of code": 7d8d5aad8261: Pulling fs layer
Step #1 - "Compare current and the last version of code": b0cd80bbbbf2: Pulling fs layer
Step #1 - "Compare current and the last version of code": 3524745a8058: Pulling fs layer
Step #1 - "Compare current and the last version of code": 96e7c89157d9: Pulling fs layer
Step #1 - "Compare current and the last version of code": 2aa84299a7d5: Pulling fs layer
Step #1 - "Compare current and the last version of code": 5e296e6769f5: Waiting
Step #1 - "Compare current and the last version of code": 7d8d5aad8261: Waiting
Step #1 - "Compare current and the last version of code": b0cd80bbbbf2: Waiting
Step #1 - "Compare current and the last version of code": 3524745a8058: Waiting
Step #1 - "Compare current and the last version of code": 96e7c89157d9: Waiting
Step #1 - "Compare current and the last version of code": 2aa84299a7d5: Waiting
Step #1 - "Compare current and the last version of code": 64a12208b46c: Waiting
Step #1 - "Compare current and the last version of code": 93be85e41fbb: Verifying Checksum
Step #1 - "Compare current and the last version of code": 93be85e41fbb: Download complete
Step #1 - "Compare current and the last version of code": 066c91c7b62c: Verifying Checksum
Step #1 - "Compare current and the last version of code": 066c91c7b62c: Download complete
Step #1 - "Compare current and the last version of code": ddf874abf16c: Verifying Checksum
Step #1 - "Compare current and the last version of code": ddf874abf16c: Download complete
Step #1 - "Compare current and the last version of code": 64a12208b46c: Verifying Checksum
Step #1 - "Compare current and the last version of code": 64a12208b46c: Download complete
Step #1 - "Compare current and the last version of code": b0cd80bbbbf2: Verifying Checksum
Step #1 - "Compare current and the last version of code": b0cd80bbbbf2: Download complete
Step #1 - "Compare current and the last version of code": 3524745a8058: Download complete
Step #1 - "Compare current and the last version of code": 96e7c89157d9: Verifying Checksum
Step #1 - "Compare current and the last version of code": 96e7c89157d9: Download complete
Step #1 - "Compare current and the last version of code": 2aa84299a7d5: Verifying Checksum
Step #1 - "Compare current and the last version of code": 2aa84299a7d5: Download complete
Step #1 - "Compare current and the last version of code": 7d8d5aad8261: Verifying Checksum
Step #1 - "Compare current and the last version of code": 7d8d5aad8261: Download complete
Step #1 - "Compare current and the last version of code": ddf874abf16c: Pull complete
Step #1 - "Compare current and the last version of code": 066c91c7b62c: Pull complete
Step #1 - "Compare current and the last version of code": 93be85e41fbb: Pull complete
Step #1 - "Compare current and the last version of code": 64a12208b46c: Pull complete
Step #1 - "Compare current and the last version of code": 5e296e6769f5: Verifying Checksum
Step #1 - "Compare current and the last version of code": 5e296e6769f5: Download complete
Step #1 - "Compare current and the last version of code": 5e296e6769f5: Pull complete
Step #1 - "Compare current and the last version of code": 7d8d5aad8261: Pull complete
Step #1 - "Compare current and the last version of code": b0cd80bbbbf2: Pull complete
Step #1 - "Compare current and the last version of code": 3524745a8058: Pull complete
Step #1 - "Compare current and the last version of code": 96e7c89157d9: Pull complete
Step #1 - "Compare current and the last version of code": 2aa84299a7d5: Pull complete
Step #1 - "Compare current and the last version of code": Digest: sha256:a36aa891d1b794641f92a27dc20cdeae7f6961d94c5b6586cc33a3a927d55ec4
Step #1 - "Compare current and the last version of code": Status: Downloaded newer image for europe-docker.pkg.dev/coffee-and-codey/codey-builder-image/codey:latest
Step #1 - "Compare current and the last version of code": europe-docker.pkg.dev/coffee-and-codey/codey-builder-image/codey:latest
Step #1 - "Compare current and the last version of code": total 16
Step #1 - "Compare current and the last version of code": drwxr-xr-x 2 root root 4096 Sep 26 16:58 codey-builder-image
Step #1 - "Compare current and the last version of code": -rw-r--r-- 1 root root  222 Sep 26 16:58 code.py
Step #1 - "Compare current and the last version of code": -rw-r--r-- 1 root root 1418 Sep 26 16:58 cloudbuild.yaml
Step #1 - "Compare current and the last version of code": -rw-r--r-- 1 root root  221 Sep 26 16:58 README.md
Step #1 - "Compare current and the last version of code": diff --git a/code.py b/code.py
Step #1 - "Compare current and the last version of code": index e69de29..073f902 100644
Step #1 - "Compare current and the last version of code": --- a/code.py
Step #1 - "Compare current and the last version of code": +++ b/code.py
Step #1 - "Compare current and the last version of code": @@ -0,0 +1,16 @@
Step #1 - "Compare current and the last version of code": +def min(a, b):
Step #1 - "Compare current and the last version of code": +  """
Step #1 - "Compare current and the last version of code": +  Calculates the minimum of two numbers.
Step #1 - "Compare current and the last version of code": +
Step #1 - "Compare current and the last version of code": +  Args:
Step #1 - "Compare current and the last version of code": +    a: The first number.
Step #1 - "Compare current and the last version of code": +    b: The second number.
Step #1 - "Compare current and the last version of code": +
Step #1 - "Compare current and the last version of code": +  Returns:
Step #1 - "Compare current and the last version of code": +    The smaller of the two numbers.
Step #1 - "Compare current and the last version of code": +  """
Step #1 - "Compare current and the last version of code": +
Step #1 - "Compare current and the last version of code": +  if a < b:
Step #1 - "Compare current and the last version of code": +    return a
Step #1 - "Compare current and the last version of code": +  else:
Step #1 - "Compare current and the last version of code": +    return b
Step #1 - "Compare current and the last version of code": \ No newline at end of file
Finished Step #1 - "Compare current and the last version of code"
Starting Step #2 - "Changes Made (for debugging)"
Step #2 - "Changes Made (for debugging)": Already have image (with digest): europe-docker.pkg.dev/coffee-and-codey/codey-builder-image/codey:latest
Step #2 - "Changes Made (for debugging)": diff --git a/code.py b/code.py
Step #2 - "Changes Made (for debugging)": index e69de29..073f902 100644
Step #2 - "Changes Made (for debugging)": --- a/code.py
Step #2 - "Changes Made (for debugging)": +++ b/code.py
Step #2 - "Changes Made (for debugging)": @@ -0,0 +1,16 @@
Step #2 - "Changes Made (for debugging)": +def min(a, b):
Step #2 - "Changes Made (for debugging)": +  """
Step #2 - "Changes Made (for debugging)": +  Calculates the minimum of two numbers.
Step #2 - "Changes Made (for debugging)": +
Step #2 - "Changes Made (for debugging)": +  Args:
Step #2 - "Changes Made (for debugging)": +    a: The first number.
Step #2 - "Changes Made (for debugging)": +    b: The second number.
Step #2 - "Changes Made (for debugging)": +
Step #2 - "Changes Made (for debugging)": +  Returns:
Step #2 - "Changes Made (for debugging)": +    The smaller of the two numbers.
Step #2 - "Changes Made (for debugging)": +  """
Step #2 - "Changes Made (for debugging)": +
Step #2 - "Changes Made (for debugging)": +  if a < b:
Step #2 - "Changes Made (for debugging)": +    return a
Step #2 - "Changes Made (for debugging)": +  else:
Step #2 - "Changes Made (for debugging)": +    return b
Step #2 - "Changes Made (for debugging)": \ No newline at end of file
Finished Step #2 - "Changes Made (for debugging)"
Starting Step #3 - "Using AI to generate documentation"
Step #3 - "Using AI to generate documentation": Already have image (with digest): europe-docker.pkg.dev/coffee-and-codey/codey-builder-image/codey:latest
Step #3 - "Using AI to generate documentation": Response from Model: The function `min` takes two numbers as input and returns the smaller of the two numbers.
Step #3 - "Using AI to generate documentation": 
Step #3 - "Using AI to generate documentation": The function first checks if `a` is less than `b`. If it is, then the function returns `a`. Otherwise, the function returns `b`.
Finished Step #3 - "Using AI to generate documentation"
Starting Step #4 - "Using AI to generate release notes"
Step #4 - "Using AI to generate release notes": Already have image (with digest): europe-docker.pkg.dev/coffee-and-codey/codey-builder-image/codey:latest
Step #4 - "Using AI to generate release notes": The generated release notes are: 
Step #4 - "Using AI to generate release notes": 
Step #4 - "Using AI to generate release notes": **Version 0.1.0**
Step #4 - "Using AI to generate release notes": 
Step #4 - "Using AI to generate release notes": * Added a new function `min()` to calculate the minimum of two numbers.
Step #4 - "Using AI to generate release notes": 
Step #4 - "Using AI to generate release notes": * The function takes two arguments, `a` and `b`, which are the two numbers to be compared.
Step #4 - "Using AI to generate release notes": 
Step #4 - "Using AI to generate release notes": * The function returns the smaller of the two numbers.
Finished Step #4 - "Using AI to generate release notes"
Starting Step #5 - "Using AI to suggest optimisation"
Step #5 - "Using AI to suggest optimisation": Already have image (with digest): europe-docker.pkg.dev/coffee-and-codey/codey-builder-image/codey:latest
Step #5 - "Using AI to suggest optimisation": A possible optimsation is : 
Step #5 - "Using AI to suggest optimisation": 
Step #5 - "Using AI to suggest optimisation": Sure, here is the optimized code:
Step #5 - "Using AI to suggest optimisation": 
Step #5 - "Using AI to suggest optimisation": ```python
Step #5 - "Using AI to suggest optimisation": def min(a, b):
Step #5 - "Using AI to suggest optimisation":   """
Step #5 - "Using AI to suggest optimisation":   Calculates the minimum of two numbers.
Step #5 - "Using AI to suggest optimisation": 
Step #5 - "Using AI to suggest optimisation":   Args:
Step #5 - "Using AI to suggest optimisation":     a: The first number.
Step #5 - "Using AI to suggest optimisation":     b: The second number.
Step #5 - "Using AI to suggest optimisation": 
Step #5 - "Using AI to suggest optimisation":   Returns:
Step #5 - "Using AI to suggest optimisation":     The smaller of the two numbers.
Step #5 - "Using AI to suggest optimisation":   """
Step #5 - "Using AI to suggest optimisation":   return a if a < b else b
Step #5 - "Using AI to suggest optimisation": ```
Step #5 - "Using AI to suggest optimisation": 
Step #5 - "Using AI to suggest optimisation": This code is more efficient because it uses the `if` statement to return the
Step #5 - "Using AI to suggest optimisation": smaller of the two numbers directly, rather than calling the `return` statement
Step #5 - "Using AI to suggest optimisation": twice.
Finished Step #5 - "Using AI to suggest optimisation"
Starting Step #6 - "Using AI to suggest security optimisation"
Step #6 - "Using AI to suggest security optimisation": Already have image (with digest): europe-docker.pkg.dev/coffee-and-codey/codey-builder-image/codey:latest
Step #6 - "Using AI to suggest security optimisation": A possible security optimsation is : 
Step #6 - "Using AI to suggest security optimisation": 
Step #6 - "Using AI to suggest security optimisation": The code you provided can be optimized by removing the unnecessary else statement. The following code is equivalent to the original code, but it is more efficient:
Step #6 - "Using AI to suggest security optimisation": 
Step #6 - "Using AI to suggest security optimisation": ```
Step #6 - "Using AI to suggest security optimisation": def min(a, b):
Step #6 - "Using AI to suggest security optimisation":   """
Step #6 - "Using AI to suggest security optimisation":   Calculates the minimum of two numbers.
Step #6 - "Using AI to suggest security optimisation": 
Step #6 - "Using AI to suggest security optimisation":   Args:
Step #6 - "Using AI to suggest security optimisation":     a: The first number.
Step #6 - "Using AI to suggest security optimisation":     b: The second number.
Step #6 - "Using AI to suggest security optimisation": 
Step #6 - "Using AI to suggest security optimisation":   Returns:
Step #6 - "Using AI to suggest security optimisation":     The smaller of the two numbers.
Step #6 - "Using AI to suggest security optimisation":   """
Step #6 - "Using AI to suggest security optimisation":   if a < b:
Step #6 - "Using AI to suggest security optimisation":     return a
Step #6 - "Using AI to suggest security optimisation":   else:
Step #6 - "Using AI to suggest security optimisation":     return b
Step #6 - "Using AI to suggest security optimisation": ```
Finished Step #6 - "Using AI to suggest security optimisation"
PUSH
DONE
```


# Backlog

* Detail IAM requirements in README.md
* Create IaC fast create to get a demo environment up and running rapiding and consitently
* improved documentation
* improvements based on initial feedback
* incorporate an example that takes a Python2 app and converts to a Python3 app
* Improve the narrative and demo around documentaion and code review taking data from the DORA State of DevOps Report 2023